

/*	This is a sample Program
	Use File->Load C Prog to
	load a different Program
*/

task main()
{
	// We have to declare the motor[x] index we will assign to the left and right motors. motor[0] is unused in this example. 
	int leftMotor = 1;
	int rightMotor = 2;
	
	
	// Move forward for 5 seconds
	setMotorSpeed(leftMotor, 90);	//Set the leftMotor (motor1) to half power forward (50)
	setMotorSpeed(rightMotor , 65); 	//Set the rightMotor (motor2) to half power forward (50)
	sleep(2000);			//Wait for 5 seconds before continuing on in the program.
	
	// Turn left at full power for 0.75 seconds
	setMotorSpeed(leftMotor, 50);	//Set the leftMotor (motor1) to full power forward (50)
	setMotorSpeed(rightMotor, 25);//Set the rightMotor (motor2) to full power reverse (-50)
	sleep(2000);			//Wait for .81 seconds
	
	
	// Move forward for 5 seconds
	setMotorSpeed(leftMotor, 90);	
	setMotorSpeed(rightMotor , 65);	
	sleep(1400);			//Wait for 5 seconds before continuing on in the program.
	
	
	// Turn left at full power for 0.75 seconds
	setMotorSpeed(leftMotor, 80);	//Set the leftMotor (motor1) to full power forward (50)
	setMotorSpeed(rightMotor, 30);//Set the rightMotor (motor2) to full power reverse (-50)
	sleep(300);			//Wait for .81 seconds
	
	
	
	
	
	
}
