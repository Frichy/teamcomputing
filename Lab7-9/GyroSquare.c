int leftMotor = 2;
int rightMotor = 1;
int Gyro = 3;

const float DEGREESTOCM = 360/17.3;

float distanceInDegrees(long distance)
{
	return distance*DEGREESTOCM;
}

void turn90Left()
{
	wait1Msec(1000);
	resetGyro(3);
	wait1Msec(1000);
	
	while(abs(getGyroDegrees(Gyro))<90)
	{
		motor[leftMotor] = -20;
		motor[rightMotor] = 20; 	
	}
	
	motor[leftMotor] = 0;
	motor[rightMotor] = 0;
	wait1Msec(1000);
	
	
	
}

void turn90Right()
{
	wait1Msec(1000);
	resetGyro(3);
	wait1Msec(1000);
	
	while(abs(getGyroDegrees(Gyro))<90)
	{
		motor[leftMotor] = 20;
		motor[rightMotor] = -20; 
		
	}
	
	motor[leftMotor] = 0;
	motor[rightMotor] = 0;
	wait1Msec(1000);
	
}

void square(long dist, long power)
{
	float degreesToTurn = 0;
	degreesToTurn = distanceInDegrees(dist);
	displayBigTextLine(6,"Power %d",power);
	
	for (int i = 0; i<=3; i++){
		setMotorSyncEncoder(leftMotor, rightMotor, 0, degreesToTurn, power);
		wait1Msec(2000);
		turn90Left();
	}
	
	
}

task main()
{
	SensorType[S4] = sensorEV3_Gyro;
	long distance = 0;
	long power = 0;
	
	displayTextLine(1,"Lab7 GyroSquare");
	
	wait1Msec(2000);
	resetGyro(Gyro);
	wait1Msec(2000);
	
	setLEDColor(ledRedFlash);
	
	while (getButtonPress(buttonEnter)== 0)
	{
		displayCenteredBigTextLine(6, "Dist is %d cm", distance);
		wait1Msec(200);
		if(getButtonPress(buttonUp))
			distance = 10;
		else if(getButtonPress(buttonDown))
			distance = 20;
		else if(getButtonPress(buttonLeft))
			distance = 25;
		else if(getButtonPress(buttonRight))
			distance = 40;
		
		
		
	}
	
	setLEDColor(ledGreenFlash);
	
	displayCenteredBigTextLine(4, "Dist = %d cm",distance);
	power = random(50);
	square(distance, power);
	
}
