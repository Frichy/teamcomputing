int leftMotor = 2;
int rightMotor = 1;
int Gyro = 3;


const float DEGREESTOCM = 360/17.3;

float distanceInDegrees(long distance)
{
	return distance*DEGREESTOCM;
}

void turn60Left()
{
	wait1Msec(1000);
	resetGyro(3);
	wait1Msec(1000);
	
	while(abs(getGyroDegrees(Gyro))<120)
	{
		motor[leftMotor] = -20;
		motor[rightMotor] = 20; 	
	}
	
	motor[leftMotor] = 0;
	motor[rightMotor] = 0;
	wait1Msec(1000);
	
	
	
}
