int leftMotor = 1;
int rightMotor = 2;

void drive(long nMotorRatio, long dist, long power)
{
	resetMotorEncoder(leftMotor);
	resetMotorEncoder(rightMotor);
	//0 on next call means same direction, same speed
	setMotorSyncEncoder(leftMotor,rightMotor,0,900, 50);//5 revolutions inhes
	wait1Msec(1000);//motor speeds will go to 0
	
}


void turn90(long nMotorRatio, long power)
{
	resetMotorEncoder(leftMotor);
	resetMotorEncoder(rightMotor);
	
	setMotorSyncEncoder(leftMotor,rightMotor,45, 1500,50);
	wait1Msec(900);
	
}
task main()
{
	drive(0,1800,50);
	turn90(50,50);
}
