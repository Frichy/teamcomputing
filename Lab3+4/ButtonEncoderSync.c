int leftMotor = 1;
int rightMotor = 2;

const float DEGTOCM = 20.8;

float DISTinDEG(long distance)
{
	return DEGTOCM*distance;
}

void drive(long nMotorRatio, long dist, long power)
{
	float degreesToTurn = 0;
	degreesToTurn = DISTinDEG(dist);
	nMotorEncoder[leftMotor]=0;
	
	while(nMotorEncoder[leftMotor] < degreesToTurn)
	{
		motor[leftMotor] = power;
		motor[rightMotor] = power;
	}
	setMotorSpeed(leftMotor, 0);
	setMotorSpeed(rightMotor, 0);
}

task main()
{
	
	
	displayCenteredTextLine(1, "Pressed button:");
	// Loop forever
	
	while (true)
	{
		
		if (getButtonPress(buttonUp)) 
			drive(0,10,50);
		else if (getButtonPress(buttonDown))
			drive(0,40,50);
		else if (getButtonPress(buttonRight))
			drive(0,60,50);
		else if (getButtonPress(buttonLeft))
			drive(0,80,50);
		
		
		// Wait 20 ms, this gives us 50 readings per second
		sleep(20);
	}
	
	
}
