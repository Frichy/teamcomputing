//declaring variables 
int leftMotor = 1;
int rightMotor = 2;
// declaring functions 

void turn90degreesLeft()
{
	setMotorSpeed(leftMotor ,50);
	setMotorSpeed(rightMotor ,-50);
	wait1Msec(450);
}

void turn90degreesRight()
{
	setMotorSpeed(leftMotor ,-50);
	setMotorSpeed(rightMotor ,50);
	wait1Msec(450);
}

void goForward1second()
{
	setMotorSpeed(leftMotor ,50);
	setMotorSpeed(rightMotor ,50);
	wait1Msec(1000);
}

void swingRight90degrees()
{
	setMotorSpeed(leftMotor ,0);
	setMotorSpeed(rightMotor ,50);
	wait1Msec(450);
}

void swingLeft90degrees()
{
	setMotorSpeed(leftMotor ,50);
	setMotorSpeed(rightMotor ,-0);
	wait1Msec(450);
}

void reverse1second()
{
	setMotorSpeed(leftMotor ,-50);
	setMotorSpeed(rightMotor ,-50);
	wait1Msec(1000);
}
// end functions

//main telling program what to do in what order using functions 
task main()
{
	
	turn90degreesLeft();
	turn90degreesRight();
	goForward1second();
	swingRight90degrees();
	swingLeft90degrees();
	reverse1second();
}
// end program 