//setting up motors
int leftMotor = 1; 
int rightMotor = 2;
// move Robot function 
void moveRobot(long nMotorRatio, long time, long power)
{
	setMotorSyncTime(leftMotor, rightMotor, nMotorRatio, time, power);
	sleep(1500);
}

task main()
{
	moveRobot(0, 1000, 50); //straight 
	moveRobot(38, 1000, 50); //sharp turn left
	moveRobot(0, 1000, 50); //straight 
	moveRobot(38, 1000, 50); //sharp turn left
	moveRobot(0, 1000, 50); //straight 
	moveRobot(38, 1000, 50); //sharp turn left
	moveRobot(0, 1000, 50); //straight 
	moveRobot(38, 1000, 50); //sharp turn left
	
	moveRobot(0, 2000, random(60)); //sharp turn left
	moveRobot(100, 3000, 72); //180deg turn 
	moveRobot(0, 2000, random(60)); //sharp turn left
	
	
	
	
	
	
	sleep(1000);
}
